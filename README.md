# REST API
> 2021-12-17 Leandro Silva


## Deploy

activate profile  
`set spring_profiles_active=<profile>`

local run  
`mvnw.cmd spring-boot:run`

local run with profiles   
`mvnw.cmd spring-boot:run -Dspring-boot.run.profiles=<profile>`

run final solution  
`java -jar target/rest_api-<version>.jar --spring.profiles.active=<profile>`

---

## Build

clean target  
`mvnw.cmd clean`

target build  
`mvnw.cmd package`

Docker build  
`git clone https://gitlab.com/f2044/spring_rest_api.git`
``cd spring-rest-api`
`docker build -t spring-rest-api:latest .`

## CI/CD

`sudo apk add gitlab-runner`

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "985amwJWj2rK73vtPGx9" \
  --executor "shell" \
  --description "docker-lab_node1" \
  --tag-list "develop_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"