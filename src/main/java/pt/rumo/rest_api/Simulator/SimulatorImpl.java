package pt.rumo.rest_api.Simulator;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.annotations.Expose;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimulatorImpl implements Simulator {

    @Autowired
    private SimulatorComponent setup;

    @Expose
    private boolean blink = false;

    @Expose
    private final AtomicInteger counter = new AtomicInteger();
    
    private long lastBlinkTime;

    @Expose
    private float random;

    public SimulatorImpl() {
        this.lastBlinkTime = System.currentTimeMillis();
    }

    @Override
    public String getBlink() {
        long time = System.currentTimeMillis();
        long diff = time - this.lastBlinkTime;
        if (diff > setup.getBlinkMillis()) {
            blink = !blink; // invert blink
            this.lastBlinkTime =  time;  // reset timer
        } 
        return String.valueOf(blink);
    }

    @Override
    public String getCounter() {
        return String.valueOf(counter.getAndIncrement());
    }

    @Override
    public String getRandom() {
        this.random = Math.round(Math.random() * setup.getMaxRandom() * 100) / 100f;
        return String.valueOf(this.random);
    }

    @Override
    public void refresh(){
        this.getBlink();
        this.getCounter();
        this.getRandom();
    }

    public static void main(String args[]) {
        Simulator sim = new SimulatorImpl();
        // debug
        System.out.println(sim);
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(sim);
    }

    @Override
    public String toString() {
        return "Simulator [blink=" + getBlink() + ", counter=" + getCounter() + ", random=" + getRandom() + "]";
    }
}

