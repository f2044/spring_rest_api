package pt.rumo.rest_api.Simulator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimulatorController {

    @Autowired
    private Simulator sim;

    @GetMapping("/simulator")
    public String getSimValues() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        sim.refresh(); //refresh
        return gson.toJson(sim);
    }
}
