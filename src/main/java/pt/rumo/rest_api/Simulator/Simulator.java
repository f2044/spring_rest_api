package pt.rumo.rest_api.Simulator;

public interface Simulator {
    
    public String getBlink();
    
    public String getCounter();

    public String getRandom();

    public void refresh();
}
