package pt.rumo.rest_api.Simulator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class SimulatorComponent {
    
    @Value("100")
    private int maxRandom;
    @Value("3000")
    private int blinkMillis;

    
}
